import logging
import sys
import unittest
from typing import List

import numpy as np

logging.basicConfig(stream=sys.stderr)


class ConvTestFunctional(unittest.TestCase):

    def setUp(self):
        self.logger = logging.getLogger("FuncTests")
        self.logger.setLevel(logging.DEBUG)

    def test_single_channel_no_padding(self):
        A = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=3, w=3, cin=1))

        W = np.array((
            [0.5, 1],
            [2, 2.5]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        bias = [3]
        padding = 0
        stride = 1

        output = np.array((
            [26, 32],
            [44, 50]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_max_conv(self):
        A = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=3, w=3, cin=1))

        kernel_val = 2
        W = np.array((
            [kernel_val]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=1, cin=1, cout=1))

        bias = [0]
        padding = 10
        stride = 1

        output = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        output = output * kernel_val
        output = np.pad(output, padding, mode='constant')

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_single_channel_no_padding_double_batch(self):

        # N: size of the batch. A may contain many matrices, each should be processed individually
        # CASE: N > 1, many inputs, output size being proportional to N

        A0 = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        A1 = np.array((
            [1, 2, 3],
            [4, 512, 6],
            [7, 8, 9]), dtype="float")

        input_tensor = InputTensor(data=[A0, A1], meta=InputTensorMetadata(n=2, h=3, w=3, cin=1))

        W = np.array((
            [0.5, 1],
            [2, 2.5]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        bias = [3]
        padding = 0
        stride = 1

        output = [
            np.array((
                [26, 32],
                [44, 50]), dtype="float"),
            np.array((
                [1293.5, 1046],
                [551, 303.5]), dtype="float")]

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning two elements
        self.assertEqual(len(result), 2, msg="Verify output contains two elements")

        # Output array equal expected
        self.assertTrue(all(np.array_equal(res, out) for res, out in zip(result, output)))

    def test_no_padding_two_input_channels(self):

        # CIN describes of how many channels current batch consists of
        # CASE: N = 1, Cin > 1. Many channels. Summing convolutions for channel. One output matrix accordingly.

        A = np.array([
            [
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]
            ],
            [
                [11, 3, 2],
                [4, 15, 6],
                [1, 8, 17],
            ]
        ], dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=3, w=3, cin=2))

        W = np.array((
            [
                [0.5, 1],
                [2, 2.5]
            ],
            [
                [-0.5, -1],
                [-2, -2.5]
            ]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=2, cout=1))

        bias = [3]
        padding = 0
        stride = 1

        output = np.array((
            [-28, -16.5],
            [5, -22]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains two elements")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_single_channel_padding(self):
        A = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=3, w=3, cin=1))

        W = np.array((
            [0.5, 1],
            [2, 2.5]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        bias = [3]
        padding = 1
        stride = 1

        output = np.array((
            [5.5,  10,   14.5, 9],
            [14,   26,   32,   16.5],
            [24.5, 44,   50,   24],
            [10,   14.5, 16,   7.5]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_dual_out_channel_padding(self):
        A = np.array((
            [1, 2, 3],
            [4, 5, 6]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=2, w=3, cin=1))

        W0 = np.array((
            [0.5, 1],
            [2, 2.5]), dtype="float")

        W1 = np.array((
            [-0.5, -1],
            [-2, -2.5]), dtype="float")

        filter = Filter(data=[W0, W1], meta=FilterMetadata(f=2, cin=1, cout=2))

        bias = [3, -3]
        padding = 0
        stride = 1

        output = [np.array(([26, 32]), dtype="float"),
                  np.array(([-26, -32]), dtype="float")]

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning two elements
        self.assertEqual(len(result), 2, msg="Verify output contains two elements")

        # Output array equal expected
        self.assertTrue(all(np.array_equal(res, out) for res, out in zip(result, output)))

    def test_stride(self):
        A = np.array((
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=4, w=4, cin=1))

        W = np.array((
            [1, 2],
            [3, 4]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        bias = [2]
        padding = 0
        stride = 2

        output = np.array((
            [46, 66],
            [126, 146]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_stride_2(self):
        A = np.array((
            [1, 2, 3, 4, 5],
            [6, 7, 8, 9, 10],
            [11, 12, 13, 14, 15],
            [16, 17, 18, 19, 20]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=5, w=4, cin=1))

        W = np.array((
            [1, 2],
            [3, 4]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        bias = [2]
        padding = 0
        stride = 2

        output = np.array((
            [53, 73],
            [153, 173]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_padd_filter3x3(self):
        A = np.array((
            [1, 2, 3, 4, 5],
            [6, 7, 8, 9, 10],
            [11, 12, 13, 14, 15],
            [16, 17, 18, 19, 20]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=5, w=4, cin=1))

        W = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=3, cin=1, cout=1))

        bias = [2]
        padding = 1
        stride = 1

        output = np.array((
            [130, 204, 243, 282, 186],
            [278, 413, 458, 503, 320],
            [443, 638, 683, 728, 455],
            [242, 333, 354, 375, 222]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))

    def test_padd_stride_filter3x3(self):
        A = np.array((
            [1, 2, 3, 4, 5],
            [6, 7, 8, 9, 10],
            [11, 12, 13, 14, 15],
            [16, 17, 18, 19, 20]), dtype="float")

        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=5, w=4, cin=1))

        W = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        filter = Filter(data=[W], meta=FilterMetadata(f=3, cin=1, cout=1))

        bias = [2]
        padding = 1
        stride = 2

        output = np.array((
            [130, 243, 186],
            [443, 683, 455]), dtype="float")

        conv_layer = ConvLayer()
        result = conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

        # Output contaning one element
        self.assertEqual(len(result), 1, msg="Verify output contains one element")

        # Output array equal expected
        self.assertTrue(np.array_equal(result.pop(), output))


class ConvTestApi(unittest.TestCase):

    def setUp(self):
        self.logger = logging.getLogger("API")
        self.logger.setLevel(logging.DEBUG)

    def test_invalid_input_batch(self):
        A, W, bias, padding, stride = self.generic_input()

        conv_layer = ConvLayer()
        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=2, h=3, w=3, cin=2))
        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        with self.assertRaises(ValueError):
            conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

    def test_invalid_input_cin(self):
        A, W, bias, padding, stride = self.generic_input()

        conv_layer = ConvLayer()
        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=3, w=3, cin=2))
        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=1))

        with self.assertRaises(ValueError):
            conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

    def test_invalid_input_cout(self):
        A, W, bias, padding, stride = self.generic_input()

        conv_layer = ConvLayer()
        input_tensor = InputTensor(data=[A], meta=InputTensorMetadata(n=1, h=3, w=3, cin=1))
        filter = Filter(data=[W], meta=FilterMetadata(f=2, cin=1, cout=2))

        with self.assertRaises(ValueError):
            conv_layer.forward(A=input_tensor, W=filter, b=bias, pad=padding, stride=stride)

    @staticmethod
    def generic_input():
        A = np.array((
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]), dtype="float")

        W = np.array((
            [0.5, 1],
            [2, 2.5]), dtype="float")

        bias = [3]
        padding = 0
        stride = 1

        return A, W, bias, padding, stride


class InputTensorMetadata():

    def __init__(self, n, h, w, cin):
        """
        Metadata describing the input tensor
        :param n:   size of the batch of data to process
        :param h:   height of the input data
        :param w:   width of the input data
        :param cin: number of input channels
        """

        self.n = n
        self.h = h
        self.w = w
        self.cin = cin


class InputTensor(object):

    def __init__(self, data: List[np.array], meta: InputTensorMetadata):
        """
        The input tesnsor
        :param data: Raw data
        :param meta: Metadata describing input
        """
        self.A = data
        self.meta = meta

    def __str__(self):
        return "Batches:{}, input HxW:{}x{}, input_chan: {}".format(self.meta.n, self.meta.h, self.meta.w, self.meta.cin)


class FilterMetadata(object):

    def __init__(self, f, cin, cout):
        """
        Metadata describing the filter
        :param f:    size of the filter F x F
        :param cin:  number of input channels
        :param cout: number of output channels
        """

        self.f = f
        self.cin = cin
        self.cout = cout


class Filter(object):

    def __init__(self, data: List[np.array], meta: FilterMetadata):
        """
        The input tesnsor
        :param data: Raw data
        :param meta: Metadata describing input
        """
        self.W = data
        self.meta = meta

    def __str__(self):
        return "F:{}x{}, input_chan: {}, output_chan: {}".format(self.meta.f, self.meta.f, self.meta.cin, self.meta.cout)


# noinspection PyPep8Naming
class ConvLayer(object):

    def __init__(self):
        self.logger = logging.getLogger("ConvLayer")
        self.logger.setLevel(logging.DEBUG)

    def forward(self, A: InputTensor, W : Filter, b: List[int], pad, stride) -> List[np.array]:
        return self._forward(A, W, b, pad, stride)

    def _forward(self, input_tensor, kernel, bias, pad, stride=1) -> List[np.array]:

        self.validate_input(input_tensor, kernel, bias)

        # 1 Pad the input data
        input_tensor.A = [np.pad(input_data, pad, mode='constant') for input_data in input_tensor.A]

        # 2 main loop
        # Assign output list
        outputs = []
        conv_cnt = 0
        output_tmp = None

        for input_data in input_tensor.A:
            for kern, curr_bias in zip(kernel.W, bias):
                for channel in range(input_tensor.meta.cin):

                    c_input = input_data
                    c_kern = kern

                    if input_tensor.meta.cin > 1:
                        c_input = input_data[channel]
                        c_kern = kern[channel]

                    conv_cnt_tmp, output = self.process_channel(c_input, c_kern, pad, stride)
                    conv_cnt += conv_cnt_tmp
                    output_tmp = output if output_tmp is None or input_tensor.meta.cin <= 1 else np.add(output_tmp, output)

                # apply bias and add to output collection
                output_tmp += curr_bias
                outputs.append(output_tmp)

        self.logger.info("Finished...")
        self.logger.info("Convolutions: {}".format(conv_cnt))
        self.logger.info("Input: {}".format(input_tensor))
        self.logger.info("Kernel: {}".format(kernel))
        self.logger.info("Padding: {}, stride: {}".format(pad, stride))

        return outputs

    @staticmethod
    def process_channel(input_data, kern, pad, stride):
        conv_cnt = 0

        in_h, in_w = input_data.shape

        if kern.size > 1:
            kern_h, kern_w = kern.shape
        else:
            kern_h, kern_w = (1, 1)

        out_h = ((in_h - kern_h) // stride) + 1
        out_w = ((in_w - kern_w) // stride) + 1

        output = np.zeros((out_h, out_w), dtype="float")

        out_h += pad if stride > 1 else 0
        out_w += pad if stride > 1 else 0

        for y in range(0, out_h + stride - 1, stride):
            for x in range(0, out_w + stride - 1, stride):
                # extract the ROI of the input data set
                roi = input_data[y:y + kern_h, x:x + kern_w]

                # element wise multiplication between the ROI and the kernel, sum the resulting matrix
                conv_res = (roi * kern).sum()
                conv_cnt += 1

                # store the convolved value in the output (x, y)
                output[y // stride, x // stride] = conv_res

        # for single row matrices return single array
        return conv_cnt, output if out_h > 1 else output[0]

    @staticmethod
    def validate_input(input_tensor, kernel, bias):

        if input_tensor.meta.cin > 1:
            if any(len(inp) != input_tensor.meta.cin for inp in input_tensor.A):
                raise ValueError("Input channels number does not match data set provided")

        if len(input_tensor.A) != input_tensor.meta.n:
            raise ValueError("Input batch number does not match data set provided")

        if len(kernel.W) != kernel.meta.cout:
            raise ValueError("Output channels number does not match data set provided")

        if len(bias) != kernel.meta.cout:
            raise ValueError("Output channels number does not match bias size provided")


if __name__ == '__main__':
    unittest.main(verbosity=3)