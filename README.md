# python forward pass 2D conv layer

Created for the coding task challenge.


Test log:

test_invalid_input_batch (__main__.ConvTestApi) ... ok

test_invalid_input_cin (__main__.ConvTestApi) ... ok

test_invalid_input_cout (__main__.ConvTestApi) ... ok

test_dual_out_channel_padding (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 4
INFO:ConvLayer:Input: Batches:1, input HxW:2x3, input_chan: 1
INFO:ConvLayer:Kernel: F:2x2, input_chan: 1, output_chan: 2
INFO:ConvLayer:Padding: 0, stride: 1
ok

test_no_padding_two_input_channels (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 8
INFO:ConvLayer:Input: Batches:1, input HxW:3x3, input_chan: 2
INFO:ConvLayer:Kernel: F:2x2, input_chan: 2, output_chan: 1
INFO:ConvLayer:Padding: 0, stride: 1
ok

test_padd_filter3x3 (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 20
INFO:ConvLayer:Input: Batches:1, input HxW:5x4, input_chan: 1
INFO:ConvLayer:Kernel: F:3x3, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 1, stride: 1
ok

test_padd_stride_filter3x3 (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 6
INFO:ConvLayer:Input: Batches:1, input HxW:5x4, input_chan: 1
INFO:ConvLayer:Kernel: F:3x3, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 1, stride: 2
ok

test_single_channel_no_padding (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 4
INFO:ConvLayer:Input: Batches:1, input HxW:3x3, input_chan: 1
INFO:ConvLayer:Kernel: F:2x2, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 0, stride: 1
ok

test_single_channel_no_padding_double_batch (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 8
INFO:ConvLayer:Input: Batches:2, input HxW:3x3, input_chan: 1
INFO:ConvLayer:Kernel: F:2x2, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 0, stride: 1
ok

test_single_channel_padding (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 16
INFO:ConvLayer:Input: Batches:1, input HxW:3x3, input_chan: 1
INFO:ConvLayer:Kernel: F:2x2, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 1, stride: 1
ok

test_stride (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 4
INFO:ConvLayer:Input: Batches:1, input HxW:4x4, input_chan: 1
INFO:ConvLayer:Kernel: F:2x2, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 0, stride: 2
ok

test_stride_2 (__main__.ConvTestFunctional) ... INFO:ConvLayer:Finished...
INFO:ConvLayer:Convolutions: 4
INFO:ConvLayer:Input: Batches:1, input HxW:5x4, input_chan: 1
INFO:ConvLayer:Kernel: F:2x2, input_chan: 1, output_chan: 1
INFO:ConvLayer:Padding: 0, stride: 2
ok

----------------------------------------------------------------------
Ran 12 tests in 0.009s

OK

######Numerical complexity:
 - worst case:
    Input of size HxW, padding=P, stride=1, kernel F=fxf
    
    Computational complexity: 
    ######O((H + P\*2) \* (W + P\*2) \* f^2)
    
    Example:
    Input size 800x600, conv mask 8x8
    O(800\*600\*8\*8) = 30720000 operations